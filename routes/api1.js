const express = require('express');
//const jwt = require('jsonwebtoken');
const path = require('path');
//var Busboy = require('busboy');
const fetch = require('node-fetch');
const geolib = require('geolib');
const tramUpdateLocation = require('./tramLocationUpdate');

const tramUpdateLocation1 = require('./newTramLocationUpdate');
var moment = require('moment'); // require



//database connection
const db = require("../helper/dbhelper");
const { UV_FS_O_FILEMAP, SSL_OP_SSLEAY_080_CLIENT_DH_BUG } = require('constants');
const { json } = require('body-parser');


const router = express.Router();




//Api 1 test api
router.get("/test", async (req, res) => {

    let query = `select * from trum_stop;`;
    console.log('Query ::: ', query);

    const getAllStop = (q) => {
        return new Promise((resolve, reject) => {
            db.query(q, (err, result) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }

                resolve(result.rows);
            })
        })
    }

    const get_all_station = await getAllStop(query);
    console.log('Result ::: ', get_all_station);


    console.log('Api 1 is called successfully');
    res.json({ status: "success", message: "api 1 test success", result: get_all_station })

})



//Api for adding trum stop
router.post("/addStop", async (req, res) => {

    console.table(req.body);
    let stop_id = req.body.stop_id;
    let stop_name = req.body.stop_name;
    let latitude = req.body.latitude;
    let longitude = req.body.longitude;

    let query1 = `insert into trum_stop(stop_id, stop_name, latitude,longitude,"createdAt","updatedAt") values(${stop_id},'${stop_name}',${latitude},${longitude},NOW(),NOW());`;
    console.log('Query ::: ', query1);

    let query2 = `SELECT * from trum_stop where stop_name = '${stop_name}';`

    db.query(query2, (err1, result1) =>{
        if(result1.rowCount >= 1){
          res.send({"Message" : `Station ${stop_name} is already present! You can't add twice!`});
        }else{
            db.query(query1, (err2, result2) => {
                if(err2){
                    res.status(400).send(err);
                }else{
                    res.status(200).send({"Message" : `Stop_id ${stop_id} Station ${stop_name} is added!`});
                }
            })
        }  
    })

    // const addStop = (q) => {
    //     return new Promise((resolve, reject) => {
    //         db.query(q, (err, result) => {
    //             if (err) {
    //                 console.log(err);
    //                 reject(err);
    //             }

    //             resolve(result.rowCount);
    //         })
    //     })
    // }

    // const add_stop = await addStop(query);
    // console.log('Result ::: ',add_stop);


    console.log('Api add station is called successfully');
    //res.json({ status: "success", message: "Station Added Successfully", result: add_stop })

})

router.post("/newEta", (req, res) => {
    const from_station = req.body.from_station;
    const to_station = req.body.to_station;

    // const query1 = `SELECT stop_id from trum_stop where stop_name='${from_station}';`
    const query1 = `SELECT stop_id from trum_stop where stop_name = '${from_station}'`;
    const query2 = `SELECT stop_id from trum_stop where stop_name = '${to_station}'`;

    db.query(query1, (error1, result1) => {
        if(error1){
            res.status(400).send(error1);
        }else{
            var val1 = result1.rows[0].stop_id;
            // console.log(val);
            start_id = val1;
            // var start_id_1 = val1;
            console.log("Start Id :::" + start_id);
        }
        // console.log(start_id);
        db.query(query2, (error2, result2) => {
            if(error2){
                res.status(400).send(error2);
            }else{
                var val2 = result2.rows[0].stop_id;
                // console.log(val);
                end_id = val2;
                // var end_id_1 = val2;
                console.log("End Id :::" + end_id);
            }
            // res.status(200).send({"Start_id" : start_id, "End_id" : end_id});
        
            const query3 = `SELECT route_code, route from trum_route WHERE 
            ${start_id} = ANY(route) 
            or 
            ${end_id} = ANY(route);`

            db.query(query3, (err, result3) => {
                var routes = result3.rows;
                
                if(err){
                    res.send(err);
                }else{
                    if(start_id < end_id){
                        
                        for(var i = 0; i < routes.length; i++){
                            if(routes[i].route.includes(start_id) && routes[i].route.includes(end_id)){
                                console.log("YES");
                                if(routes[i].route.indexOf(start_id) < routes[i].route.indexOf(end_id)){
                                    var routeCode = routes[i].route_code;
                                    var eta = [];

                                    tracking_table.forEach(track => {
                                        //condition check for all in memory Active Trums route code and All in memory active trums upcoming stop = stopId
                                        if (track.routeCode === routeCode && (track.nextRoutes.includes(start_id))) {
                                            var eta_cal = 0;
                                            //var tracking = track.tracking;
                                            var nextRoutes = track.nextRoutes;
                                            console.log(nextRoutes);
                                
                                            if(nextRoutes.length === 0){
                                                // eta_cal = `Tram has reached it's destination!`;
                                            }
                                
                                            else if (nextRoutes[0] === start_id) {
                                                eta_cal = `Destination will reach soon`;
                                            }
                                            else if(nextRoutes.indexOf(start_id) == -1)
                                            {
                                                eta_cal = 'Tram just left';
                                            }
                                            else {
                                
                                                for (var i = 0; i < nextRoutes.length - 1; i++) {
                                
                                                    var a = nextRoutes[i];
                                                    var b = nextRoutes[i + 1];
                                                    console.log('A---->B', a, b);
                                
                                                    if (a === start_id)
                                                        break;
                                
                                                    time_table.forEach((t) => {
                                                        if (t.from_stop === a && t.to_stop === b) {
                                
                                                            eta_cal += t.time_in_min;
                                                            console.log('eta add', eta_cal);
                                                        }
                                                        if(t.to_stop === a && t.from_stop === b) {
                                                            eta_cal += t.time_in_min;
                                                            console.log('eta add', eta_cal);
                                                        } 
                                                    })
                                                }
                                            }
                                            
                                            var obj = { vehicleNo: track.vehicleNo, 
                                                routeCode: track.routeCode, 
                                                currentTramLocation : {
                                                    latitude : track.latitude,
                                                    longitude : track.longitude
                                                },
                                                currentStop: track.tracking,
                                                eta: eta_cal 
                                            };
                                            eta.push(obj);
                                        }
                                    });
                                }
                            }else{
                                var error_message = `No tram is available in this route (${from_station} - ${to_station}) right now`;
                                // res.status(400).send({"Message" : `No route is available from ${from_station} - ${to_station}`});
                            }
                        }
                    }
                    if(start_id > end_id){
                        for(var i = 0; i < routes.length; i++){
                            if(routes[i].route.includes(start_id) && routes[i].route.includes(end_id)){
                                console.log("Yes");
                                if(routes[i].route.indexOf(start_id) < routes[i].route.indexOf(end_id)){
                                    var routeCode = routes[i].route_code;

                                    var eta = [];

                                    tracking_table.forEach(track => {
                                        //condition check for all in memory Active Trums route code and All in memory active trums upcoming stop = stopId
                                        if (track.routeCode === routeCode && (track.nextRoutes.includes(start_id))) {
                                            var eta_cal = 0;
                                            //var tracking = track.tracking;
                                            var nextRoutes = track.nextRoutes;
                                            console.log(nextRoutes);
                                
                                            if(nextRoutes.length === 0){
                                                // eta_cal = `Tram has reached it's destination!`;
                                            }
                                
                                            else if (nextRoutes[0] === start_id) {
                                                eta_cal = `Destination will reach soon`;
                                            }
                                            else if(nextRoutes.indexOf(start_id) == -1)
                                            {
                                                eta_cal = 'Tram just left';
                                            }
                                            else {
                                
                                                for (var i = 0; i < nextRoutes.length - 1; i++) {
                                
                                                    var a = nextRoutes[i];
                                                    var b = nextRoutes[i + 1];
                                                    console.log('A---->B', a, b);
                                
                                                    if (a === start_id)
                                                        break;
                                
                                                    time_table.forEach((t) => {
                                                        if (t.from_stop === a && t.to_stop === b) {
                                
                                                            eta_cal += t.time_in_min;
                                                            console.log('eta add', eta_cal);
                                                        }
                                                        if(t.to_stop === a && t.from_stop === b) {
                                                            eta_cal += t.time_in_min;
                                                            console.log('eta add', eta_cal);
                                                        } 
                                                    })
                                                }
                                            }
                                            var obj = { vehicleNo: track.vehicleNo, 
                                                routeCode: track.routeCode,
                                                currentTramLocation : {
                                                    latitude : track.latitude,
                                                    longitude : track.longitude
                                                },
                                                currentStop: track.tracking, 
                                                eta: eta_cal 
                                            };
                                            eta.push(obj);
                                        }
                                    });
                                }
                            }else{
                                // var eta_cal = "No tram is available";
                                // var eta = [];
                                // eta.push(eta_cal);
                                // console.log(eta_cal);
                                // console.log(eta);
                                var error_message = `No tram is available in this route (${from_station} - ${to_station}) right now`;  
                                // res.status(400).send({"Message" : `No route is available from ${from_station} - ${to_station}`});
                            }
                        }
                    }
                }
                res.json({ status: "success", 
                    data: eta, 
                    message: error_message ? error_message : (eta.length === 0  ? `No tram is available in this route (${from_station} - ${to_station}) right now` : "Route found" )
                });
            })
        })
    })
})

router.get("/tramLocationUpdate", async (req, res) => {
    var data = await tramUpdateLocation1();
    res.json(data);
});



router.post("/run", async (req, res) => {
    var latestTramData = await tramUpdateLocation();
    var {latestTramDataArray, latestRouteDataArray} = await globalStaticTables()
    
    latestTramData.map((vehicle, index, thisArray) => {
        // const flag = false;
        const currentRoute = latestRouteDataArray.find(route => route.routeCode === vehicle.routeCode)
        const currentStopId = currentRoute.route.findIndex(stop => stop === vehicle.next.stop_id);
        const newVehicleData = { vehicleNo: vehicle.vehicleNo, routeCode: vehicle.routeCode, nextRoutes: currentRoute.slice(currentStopId), tracking: currentStopId };
        console.log(newVehicleData);
    })

    // return newVehicleData;
    res.json(latestTramData);
})

router.get("/run1", async (req, res) => {
    var latestTramData = await tramUpdateLocation1();
    
    tracking_table = [];
    
    for(var i = 0; i < latestTramData.length; i++){

        var vehicleNo = latestTramData[i].vehicleNo;

        // for(var j = 0; j < tracking_table.length; j++){
        //     // console.log("Vehicle....................",tracking_table[j].vehicleNo);
        //     if(vehicleNo === tracking_table[j].vehicleNo){
        //         var index = j;
        //         console.log("Valueeeeeeeeee..............", index);

        //         // console.log(track)
        //         // tracking_table.slice(index, 1);
        //         // console.log("---tracking-----------", tracking_table);
        //     }
        // }

        var routeCode = latestTramData[i].routeCode;
        var nextStop = latestTramData[i].next.stop_id;
        console.log({nextStop});
        var lat = latestTramData[i].latitude;
        var long = latestTramData[i].longitude;

        let query = `select route from trum_route where route_code = '${routeCode}';`;

        const findRoutes = (q) => {
            return new Promise((resolve, reject) => {
                db.query(q, (err, result) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }

                    resolve(result.rows[0].route);
                })
            })
        }

        const find_routes = await findRoutes(query);
        console.log('Routes ::: ', find_routes);

        //find_routes.shift();
        let msg = 'Vehicle ' + vehicleNo + ' added in RAM';

        let vehicle = {message: msg, vehicleNo: vehicleNo, routeCode: routeCode, latitude: lat, longitude: long, nextRoutes: find_routes, tracking: nextStop };
        console.log('Vehicle Info ::: ', vehicle);

        tracking_table.push(vehicle);

        // console.log(msg);
    }
    res.json(tracking_table);
})

// Run Cron Job here after 2 mins....

router.get("/getAllActiveVehical", (req, res) => {
    res.send(tracking_table);
})


router.get("/check", async (req, res) => {
    fetch("http://tram.distronix.in:3000/tram", {
        method: "post",
        headers: {
            "Content-Type": "application/json" }
        })
    .then(async response => response.json())
    .then(async (vehicle_data) => {
        const data = vehicle_data.data;
        
        // console.log(data.length);
        for(var i = 0; i < data.length; i++){
            distance_array = [];
            toSetMinDistance = [];
            // console.log("Values :::", i);
            var vehicleNo = data[i].vehicleNo;
            var routeCode = data[i].routeCode;

            var tram_lat = data[i].lastLocation.latitude;
            var tram_long = data[i].lastLocation.longitude;

            const query1 = `SELECT route from trum_route WHERE route_code = '${routeCode}';`;

            const findRoute = (q) => {
                return new Promise((resolve, reject) => {
                    db.query(q, (err, result) => {
                        if (err) {
                            console.log(err);
                            reject(err);
                        }
        
                        resolve(result.rows[0].route);
                    })
                })
            }
            const find_route = await findRoute(query1);
            // console.log(find_route);

            console.log("---------------------------------------------------------------");
            console.log(`Vehicle No : ${vehicleNo}, routeCode : ${routeCode}, tram_lat : ${tram_lat}, tram_long : ${tram_long}`);
            console.log("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
            console.log(find_route);
            console.log("---------------------------------------------------------------");

            for(var j = 0; j < find_route.length; j++){
                
                let query2 = `SELECT stop_id, latitude, longitude from trum_stop WHERE stop_id = ${find_route[j]};`;
                
                // console.log(query2);
                
                const findStopId = (q) => {
                    return new Promise((resolve, reject) => {
                        db.query(q, (err, result) => {
                            if (err) {
                                console.log(err);
                                reject(err);
                            }
                            // console.log(result.rows);
                            resolve(result.rows[0]);
                        })
                    })
                }
                let find_stop_id = await findStopId(query2);
                // console.log(find_stop_id);

                var val = geolib.isPointWithinRadius(
                    { latitude: tram_lat, longitude: tram_long },
                    { latitude: find_stop_id.latitude, longitude: find_stop_id.longitude },
                    100
                );

                if(val == true){
                    var tracking = find_stop_id.stop_id;
                    break;
                }
                else{
                    var distance = measure(tram_lat, tram_long, find_stop_id.latitude, find_stop_id.longitude);
                    distance = distance.toFixed(7);

                    toSetMinDistance.push(distance);
                    distance_array.push(find_stop_id.stop_id);
                    distance_array.push(distance);
                }

                function measure(lat1, lng1, lat2, lng2, miles) { // miles optional
                    if (typeof miles === "undefined"){miles=false;}
                    function deg2rad(deg){return deg * (Math.PI/180);}
                    function square(x){return Math.pow(x, 2);}
                    var r=6371; // radius of the earth in km
                    lat1=deg2rad(lat1);
                    lat2=deg2rad(lat2);
                    var lat_dif=lat2-lat1;
                    var lng_dif=deg2rad(lng2-lng1);
                    var a=square(Math.sin(lat_dif/2))+Math.cos(lat1)*Math.cos(lat2)*square(Math.sin(lng_dif/2));
                    var d=2*r*Math.asin(Math.sqrt(a));
                    if (miles){return d * 0.621371;} //return miles
                    else{return d;} //return km
                  }
                
                // console.log("Stop Id = " + find_stop_id.stop_id + " Distance In Meters = " + distance);
            }
            // console.log("Val" , tracking);

            if(tracking == null){
                Array.min = function( array ){
                    return Math.min.apply( Math, array );
                };
    
                var minimum = Array.min(toSetMinDistance);
                console.log("Minimum distance from Tram's present location to Station's location in KM: ",minimum);
    
                // console.log(toSetMinDistance);

                for(var k = 0; k < distance_array.length; k++){
                    if(minimum == distance_array[k]){
                        var position = k;
                        // console.log(position);
                        var tracking = distance_array[position - 1];
                        console.log("Tram is now going to stop :", tracking);
                        console.log("-----------------------------------------------");
    
                        let query = `select route from trum_route where route_code = '${routeCode}';`;
                        //Routes finding query
                        const findRoutes = (q) => {
                            return new Promise((resolve, reject) => {
                                db.query(q, (err, result) => {
                                    if (err) {
                                        console.log(err);
                                        reject(err);
                                    }
    
                                    resolve(result.rows[0].route);
                                })
                            })
                        }
    
                        const find_routes = await findRoutes(query);
                        console.log('Routes ::: ', find_routes);
    
                        //find_routes.shift();
    
                        let vehicle = { vehicleNo: vehicleNo, routeCode: routeCode, nextRoutes: find_routes, tracking: tracking };
                        console.log('Vehicle Info ::: ', vehicle);
    
                        tracking_table.push(vehicle);
    
                        let msg = 'Vehicle ' + vehicleNo + ' added';
                        console.log(msg);
                        // return res.json({ status: "success", message: msg });
                    }
                }
                console.log(distance_array);
            }else
            {
                console.log("Tram is now at stop :", tracking);
                console.log("-----------------------------------------------");

                let query = `select route from trum_route where route_code = '${routeCode}';`;
                //Routes finding query
                const findRoutes = (q) => {
                    return new Promise((resolve, reject) => {
                        db.query(q, (err, result) => {
                            if (err) {
                                console.log(err);
                                reject(err);
                            }

                            resolve(result.rows[0].route);
                        })
                    })
                }

                const find_routes = await findRoutes(query);
                console.log('Routes ::: ', find_routes);

                //find_routes.shift();

                let vehicle = { vehicleNo: vehicleNo, routeCode: routeCode, nextRoutes: find_routes, tracking: tracking };
                console.log('Vehicle Info ::: ', vehicle);

                tracking_table.push(vehicle);

                let msg = 'Vehicle ' + vehicleNo + ' added';
                console.log(msg);
            }
        }
        // console.log(tracking_table);
    })
    .catch(err => console.log(err))
})

router.get("/showRoutes", (req, res) => {
    var query = 'SELECT * FROM trum_route;'
    db.query(query, (error, result) => {
        if(error){
            res.send(error);
        }else{
            res.status(200).send(result.rows);
        }
    })
})

router.get("/updateTimeTable", async (req, res) => {
    var allLatLong = [];
    for(var i = 0; i < time_table.length; i++){
        var from_stop = time_table[i].from_stop;
        var to_stop = time_table[i].to_stop;

        // console.log({from_stop, to_stop});

        var query1 = `SELECT * FROM trum_stop WHERE stop_id=${from_stop} OR stop_id=${to_stop};`

        const findTramStopLatLong = (q) => {
            return new Promise((resolve, reject) => {
                db.query(q, (err, result) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }

                    resolve(result.rows);
                })
            })
        }

        const stationLatLong = await findTramStopLatLong(query1);
        // console.log('All Tram stops details ::: ', stationLatLong);
        allLatLong.push(stationLatLong);
    }
    // res.json(allLatLong);
    // console.log(allLatLong);
    for(var j = 0; j < allLatLong.length; j++){
        var fromStopId = allLatLong[j][0].stop_id;
        var fromLat = allLatLong[j][0].latitude;
        var fromLong = allLatLong[j][0].longitude;

        var toStopId = allLatLong[j][1].stop_id;
        var toLat = allLatLong[j][1].latitude;
        var toLong = allLatLong[j][1].longitude;
        
        console.log({fromStopId, fromLat, fromLong}, {toStopId, toLat, toLong});
        // From here I would need the api key for updating the time_table for one tram_stop to another tram_stop
        // 
        var timeInMin = 1;

        var query2 = `UPDATE time_table set time_in_min=${timeInMin} WHERE from_stop=${fromStopId} OR to_stop=${toStopId};`

        const updateTime = (q) => {
            return new Promise((resolve, reject) => {
                db.query(q, (err, result) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }

                    resolve(result.rows);
                })
            })
        }
        const timeUpdate = await updateTime(query2);
    }
    res.status(200).send({"Message" : "New time table set!"});
})


// Stop id 1.
// var stop_id_1 = result1.rows[0].stop_id;
// // console.log(stop_id_1);
// // Stop id 2.
// var stop_id_2 = result1.rows[1].stop_id;
// // console.log(stop_id_2);

// // res.send(result1.rows);
// const query2 = `SELECT route_code, route from trum_route WHERE 
// ${stop_id_1} = ANY(route) 
// or 
// ${stop_id_2} = ANY(route);`

// db.query(query2, (err, result2) => {
//     var routes = result2.rows;
//     // console.log(routes);
//     if(err){
//         res.send(err);
//     }else{

//     }
// })


router.post("/etaCal", async (req, res) => {
    var stopId = req.body.stopId;
    var latestTramData = await tramUpdateLocation1();

    for(var i = 0; i < latestTramData.length; i++){

    }
})


router.post("/etaCalculation", async (req, res) => {
    // var tramData = tramUpdateLocation();
    // res.send("helo");
    // var stationName = req.body.stationName;
    var stopId = req.body.stopId;
    // var query = `SELECT * from trum_stop where stop_name = '${stationName}'`;

    // const findTramStopId = (q) => {
    //     return new Promise((resolve, reject) => {
    //         db.query(q, (err, result) => {
    //             if (err) {
    //                 console.log(err);
    //                 reject(err);
    //             }
    //             resolve(result.rows[0]);
    //         })
    //     })
    // }
    // const tramDetails = await findTramStopId(query);
    // const routeCode = tramDetails.routeCode;
    // const stopId = tramDetails.stop_id;
    console.log({stopId});

    // const query1 = `SELECT route_code from trum_route WHERE ${stopId} = ANY(route)`
    
    // const findRouteCode = (q) => {
    //     return new Promise((resolve, reject) => {
    //         db.query(q, (err, result) => {
    //             if (err) {
    //                 console.log(err);
    //                 reject(err);
    //             }
    //             resolve(result.rows);
    //         })
    //     })
    // }    
    // onst routeCode = await findTramStopId(query1);

    var activeVehicleOnThisStopId = tracking_table.filter(t => t.nextRoutes.includes(stopId))

    console.log(time_table);

    const activeVehiclesWithEta = activeVehicleOnThisStopId.map(v => {
    const indexOfStopId = v.nextRoutes.findIndex(s => s === stopId)
    // const indexOfTrackingId = v.nextRoutes.findIndex(s => s === v.tracking);

    // console.log(v.nextRoutes);

    // console.log(indexOfStopId, indexOfTrackingId, v.tracking);
    // const [start, end] = indexOfTrackingId < indexOfStopId ? [indexOfTrackingId, indexOfStopId + 1] : [indexOfStopId, indexOfTrackingId + 1];

    const eta = v.nextRoutes.slice(0, indexOfStopId + 1).reduce((prev,next) => {
        // console.log({prev});
        const currentEtaObj = time_table.find(t => (t.to_stop === next && t.from_stop === prev.id) || (t.to_stop === prev.id && t.from_stop === next));
        console.log({prev, currentEtaObj});
        const currentEta = !!currentEtaObj ? currentEtaObj.time_in_min : 0;
        return {id: next, eta: prev.eta + currentEta};
    }, {id: v.tracking, eta: 0});

    console.log({eta});
    

    return {...v, eta: eta.eta}});

    res.json(activeVehiclesWithEta);
    // console.log(routeCode, stopId);
    // res.send({"Stop_id" : stopId});
})


//Api to calculate ETA
router.post("/eta", async (req, res) => {

    let routeCode = req.body.routeCode;
    let stopId = req.body.stopId;

    var eta = [];
    tracking_table.forEach(track => {
        //condition check for all in memory Active Trums route code and All in memory active trums upcoming stop = stopId
        if (track.routeCode === routeCode && (track.nextRoutes.includes(stopId))) {
            var eta_cal = 0;
            //var tracking = track.tracking;
            var nextRoutes = track.nextRoutes;
            console.log(nextRoutes);

            if(nextRoutes.length === 0){
                // eta_cal = `Tram has reached it's destination!`;
            }

            else if (nextRoutes[0] === stopId) {
                eta_cal = `will reach soon`;
            }
            else if(nextRoutes.indexOf(stopId) == -1)
            {
                eta_cal = 'just left';
            }
            else {

                for (var i = 0; i < nextRoutes.length - 1; i++) {

                    var a = nextRoutes[i];
                    var b = nextRoutes[i + 1];
                    console.log('A---->B', a, b);

                    if (a === stopId)
                        break;

                    time_table.forEach((t) => {
                        if (t.from_stop === a && t.to_stop === b) {


                            eta_cal += t.time_in_min;
                            console.log('eta add', eta_cal);
                        }
                        if(t.to_stop === a && t.from_stop === b) {
                            eta_cal += t.time_in_min;
                            console.log('eta add', eta_cal);
                        } 
                    })
                }
            }



            var obj = { vehicleNo: track.vehicleNo, routeCode: track.routeCode, tracking: track.tracking, eta: eta_cal };
            eta.push(obj);
        }

    });

    return res.json({ status: "success", data: eta });
})



//add random vehical api
router.post('/setVehical', async (req, res) => {



    let vehicleNo = req.body.vehicleNo;
    let routeCode = req.body.routeCode;

    //vahical1 current position at stop id
    let tracking = req.body.tracking;





    let query = `select route from trum_route where route_code = '${routeCode}';`;
    //Routes finding query
    const findRoutes = (q) => {
        return new Promise((resolve, reject) => {
            db.query(q, (err, result) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }

                resolve(result.rows[0].route);
            })
        })
    }

    const find_routes = await findRoutes(query);
    console.log('Routes ::: ', find_routes);

    //find_routes.shift();

    let vehical = { vehicleNo: vehicleNo, routeCode: routeCode, nextRoutes: find_routes, tracking: tracking };
    console.log('Vahical Info ::: ', vehical);

    tracking_table.push(vehical);

    let msg = 'Vehical ' + vehicleNo + ' added';
    console.log(msg);
    return res.json({ status: "success", message: msg });

})



//Api to set track position to next stop of active trum
router.get("/goNext", async (req, res) => {

    let vehicleNo = req.query.vehicleNo;

    var flag = true;

    tracking_table.forEach((t) => {
        if(t.nextRoutes.length === 1){
            t.tracking = t.nextRoutes[0];
            res.send({"Message" : `Tram ${vehicleNo} has reached it's final destination`});
            flag = false;
        }
        if (t.vehicleNo === vehicleNo && t.nextRoutes.length != 1) {
            t.tracking = t.nextRoutes[0];
            console.log('Trum set to next stop');
            flag = false;
            res.json({ status: "success", message: "trum go to next stop" })
        }
    })
    if(flag)
        return res.json({ status: "error", Message : "Tram has not started yet" });
})

// Update time table
// router.patch("/updateTimeTable", (req, res) => {
//     const updateQuery = `
//         update time_table set time_in_min = 0 where from_stop=1 and to_stop=2;
//         update time_table set time_in_min = 2 where from_stop=2 and to_stop=3;
//         update time_table set time_in_min = 3 where from_stop=3 and to_stop=4;
//         update time_table set time_in_min = 2 where from_stop=4 and to_stop=5;
//         update time_table set time_in_min = 5 where from_stop=5 and to_stop=6;
//         update time_table set time_in_min = 1 where from_stop=6 and to_stop=7;
//         update time_table set time_in_min = 5 where from_stop=7 and to_stop=8;
//         update time_table set time_in_min = 1 where from_stop=8 and to_stop=9;
//         update time_table set time_in_min = 1 where from_stop=9 and to_stop=10;
//         update time_table set time_in_min = 4 where from_stop=10 and to_stop=11;
//         update time_table set time_in_min = 6 where from_stop=11 and to_stop=12;
//         update time_table set time_in_min = 5 where from_stop=12 and to_stop=13;
//         update time_table set time_in_min = 3 where from_stop=13 and to_stop=14;
//         update time_table set time_in_min = 0 where from_stop=14 and to_stop=15;
//         update time_table set time_in_min = 4 where from_stop=15 and to_stop=16;
//         update time_table set time_in_min = 0 where from_stop=16 and to_stop=17;
//         update time_table set time_in_min = 3 where from_stop=17 and to_stop=18;
//         update time_table set time_in_min = 4 where from_stop=18 and to_stop=19;
//         update time_table set time_in_min = 3 where from_stop=19 and to_stop=20;
//         update time_table set time_in_min = 0 where from_stop=20 and to_stop=21;
//         update time_table set time_in_min = 4 where from_stop=21 and to_stop=22;
//         update time_table set time_in_min = 3 where from_stop=22 and to_stop=23;
//         update time_table set time_in_min = 5 where from_stop=23 and to_stop=24;
//         update time_table set time_in_min = 0 where from_stop=24 and to_stop=25;
//         update time_table set time_in_min = 2 where from_stop=25 and to_stop=26;
//         update time_table set time_in_min = 6 where from_stop=26 and to_stop=27;
//     `;
//     db.query(updateQuery, (err, result) => {
//         if(err){
//             res.status(400).send(err);
//         }else{
//             res.status(200).send({"Status" : "Success", "Message" : "Time table updated successfully!"});
//         }
//     })
// })


module.exports = router;

// 1-2 = 0
// 2-3 = 2
// 3-4 = 3
// 4-5 = 2
// 5-6 = 5
// 6-7 = 1
// 7-8 = 5
// 8-9 = 1
// 9-10 = 1
// 10-11 = 4
// 11-12 = 6
// 12-13 = 5
// 13-14 = 3
// 14-15 = 0 
// 15-16 = 4
// 16-17 = 0
// 17-18 = 3
// 18-19 = 4
// 19-20 = 3
// 20-21 = 0 
// 21-22 = 4
// 22-23 = 3
// 23-24 = 5
// 24-25 = 0
// 25-26 = 2
// 26-27 = 6



// console.log(query1);

            // db.query(query1, async (err1, result1) => {
            //     // var c_array = result1.rows[0];
                
                
            //     // console.log(c_array);


            //     // console.log(c_array.route.length);;

            //     // for(var j = 0; j < c_array.route.length; j++){
            //     //     let query2 = `SELECT stop_id, latitude, longitude from trum_stop WHERE stop_id = ${c_array.route[j]};`;
            //     //     console.log(query2);
            //     //     // console.log(c_array.route[j]);
            //     //     const tram = (q) => {
            //     //         return new Promise((resolve, reject) => {
            //     //             db.query(q, (err, result) => {
            //     //                 if (err) {
            //     //                     console.log(err);
            //     //                     reject(err);
            //     //                 }
            //     //                 // console.log(result.rows);
            //     //                 resolve(result.rows[0]);
            //     //             })
            //     //         })
            //     //     }
                
            //     //     let patient_master = await tram(query2);
            //     //     console.log(patient_master);
            //     //     // db.query(`SELECT stop_id, latitude, longitude from trum_stop WHERE stop_id = ${c_array.route[j]};`, (err2, result2) => {
            //     //     //     console.log(result2.rows[0]);
            //     //     // })
            //     // }
            // })



//Addd a demo Vehical or static vehical
// async function setVehical_1() {

//     const vehicleNo = `WBTC0245`;
//     const routeCode = `RT24:UP`;





//     let query = `select route from trum_route where route_code = '${routeCode}';`;
//     //Routes finding query
//     const findRoutes = (q) => {
//         return new Promise((resolve, reject) => {
//             db.query(q, (err, result) => {
//                 if (err) {
//                     console.log(err);
//                     reject(err);
//                 }

//                 resolve(result.rows[0].route);
//             })
//         })
//     }

//     const find_routes = await findRoutes(query);
//     console.log('Routes ::: ', find_routes);

//     // let tracking = find_routes[0];

//     //vahical1 current position at stop id
//     let tracking = 1;

//     //find_routes.shift();

//     let vehical_1 = { vehicleNo: vehicleNo, routeCode: routeCode, nextRoutes: find_routes, tracking: tracking };
//     console.log('Vahical 1 Info ::: ', vehical_1);

//     tracking_table.push(vehical_1);

// }
// //add static vehical 1 api for testing
// router.get('/setVehical1', async (req, res) => {

//     setVehical_1();
//     console.log('Vahical 1 added !');
//     return res.json({ status: "success", message: "vahical 1 added" });

// })
